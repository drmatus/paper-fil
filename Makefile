all:
	pdflatex main.tex
	bibtex main.aux
	pdflatex main.tex
	pdflatex main.tex
clean:
	rm -f *aux *bbl *blg *log *toc *.out *.xml *-blx.bib *.lof *.fls
