Next, hydrodynamical simulations of filaments (e.g. Seifried & Walch 2015, MNRAS 452,
2410 - see in particular their Table 2; Clarke et al. 2017, MNRAS 468, 2489) suggest that
filaments of comparable line mass density and magnetic field strengths (75 Msun/pc and
40 microGauss in the models F3 of Seifried & Walch 2015) fragment on the timescale of
a several tenths of Myr, while the 'slingshot mechanism' assumes that the filament is
unaltered for a time-scale of at least one period of filament oscillation, which is of
the order of several Myrs.  
    > As this work is the first to explore the effects of the slingshot 
    > mechanism on the survival of a cluster, we were interested in exploring 
    > the parameter space of amplitude and periods of the oscillation, choosing 
    > values that would require that the filament survives for a longer period 
    > than in the real world.
    > The effects of the slingshot in our simulations are produced within the 
    > first 1/4 of an oscillation, so even though we run the simulation for 2 
    > oscillations, the gas filament has to survive for a 1/4 of an oscillation 
    > to be able to produce the effects that we observe.  
    > In any case, it would be interesting to see what happens to the cluster 
    > when the density of the filament decreases with time.

### R: It might be of interest to mention that the slingshot mechanism does not
necessitate several oscillations, and that even 1/4th of the whole period is enough
(probably the best place where to add this is at the end of Sect. 2.1).

    > We added that result right before section 4.1, so now it now
    > appears there and in the conclusions.


####################################


Sect. 2.2: Please, describe the longitudinal (i.e. along the z axis) profile of
the filament and its time evolution.  
    > We have included a brief description of the filament along the z axis.  
    > For a fixed distance R from the centre of the filament, the value of the 
    > density is constant along the z axis.

### R: This explanation was necessary to understand the motions undertaken by
the filament.  Thus, the radial and longitudinal structure (relative to a reference
point inside the filament) is unaltered throughout the simulation, and the only time
dependence of the filament is due to the translational motion of the filament itself.
When reading the manuscript before, I thought that the ridgeline of the filament bends
both as a function of z-position and time.  It might be worth emphasizing that the
filament does not bend in your models; that it shifts instead and that the coordinate
x(t) lies in the direction perpendicular to the coordinate axis z.

    > We added Equation 4 to indicate that the density profile depends only on
    > the x and y coordinates, not on the z coordinate, more specifically on
    > the distance to the position of the ridgeline (x_fil, y_fil)

####################################


Sect. 2.3: Setting all clusters with particles of the same mass would ignore some
relaxation processes (e.g. mass segregation), which can establish on the time-scale of
question. This should be mentioned.  
    > We agree with the need to mention our reasons behind the use of same mass 
    > particles, so we have added a paragraph in the conclusions addressing 
    > this point.  
    > In our simulations, the effects of the Slingshot occur during the first 
    > 1/4 of the oscillation. In the case of the filaments with longest period 
    > that we use, that value corresponds to the first 1.25 Myr of simulation.  
    > Model A has the shortest relaxation time, with t_relax = 1.21 Myr, of our 
    > models, so we consider that there is not enough time for relaxation 
    > processes to be significant for the evolution of our clusters, as the 
    > Slingshot acts in shorter time scales.


### R: The discussion of the relaxation time-scale is appropriate for the scope
of the paper, but could you shortly comment on mass segregation, please (this
was my original question)? The timescale of mass segregation t_ms for stars of
M is shorter than t_rlx (t_ms \approx {m_mean/M} t_rlx), and thus more
relevant.  This equation can be found in Spitzer 1987 (his book on globular
cluster dynamics) or in Mouri & Taniguchi 2002, ApJ 580, 844.  I am also
wondering whether this paragraph does not fit better to Sect. 2.3 rather than
to Conclusions.
    > We added a paragraph where we mention that systems as young as the ONC
    > can be mass segregated, so adding this process to our simulations could
    > improve our results, but the effects of the slingshot on mass segregated
    > systems are outside the scope of this paper, so we leave this topic for a
    > future paper.

####################################

Sect. 3.1, line 53: The point colours in Fig. 4 do not match Table 1. Also, the authors
do not mention model C neither in the text nor in the Table caption (which model is
represented by the orange crosses?).  
    > We have fixed this oversight in the current version of the manuscript

### R: Still, the model names as described in the boldface in the first paragraph of
Sect. 4.1 do not match the models shown in the legend of Fig. 4.
    
    > Fixed text on section 4.1, now the main text and figure match


####################################


### R: Fig. 5, first sentence: the model D is now called C. Please, correct it.
Please, also make sure that the models are correctly relabelled everywhere in
the text.
    > Fixed.

####################################


Sect. 3.1, around line 42: The label 'healthy clusters' might be misleading because
some of the ejected clusters are also not significantly affected. If possible, please
suggest a more appropriate terminology (and also further down in the paper).  
    > We have replaced "healthy" with "embedded" to refer to the clusters that 
    > lose less than 20% of the initial mass.

### R: Please use another terminology than "embedded" because the term 
\"embedded clusters\" is already reserved for clusters embedded by their natal
gas (e.g. as used in the review of Laga & Lada 2003).  Would 'filament
associated cluster' be a reasonable substitute?

    > We have replaced "embedded" by "Filament associated"

####################################


Figure 7: The range of the axes for non-negative quantities (i.e. fractions) should
start from 0. Please correct it. The same holds for Figs. 9, 13 and 16.  > Done.

### R: This point has not been taken into account.

    > Corrected

####################################

### R: Caption to Fig. 7: There is no blue dashed line. The same holds for
the caption of Fig. 9. Please, correct it.
    > Corrected
